# Marc Riera <marcriera@softcatala.org>, 2020.
#
msgid ""
msgstr "Project-Id-Version: calls\n"
"Report-Msgid-Bugs-To: https://source.puri.sm/Librem5/calls/issues\n"
"POT-Creation-Date: 2020-07-18 15:24+0000\n"
"PO-Revision-Date: 2020-07-18 18:45+0200\n"
"Last-Translator: Marc Riera <marcriera@softcatala.org>\n"
"Language-Team: Catalan\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/calls-application.c:310 src/ui/call-window.ui:11 src/ui/main-window.ui:8
msgid "Calls"
msgstr "Trucades"

#: src/calls-application.c:519
msgid "The name of the plugin to use for the call Provider"
msgstr "Nom del connector que s'utilitzarà per al proveïdor de trucades"

#: src/calls-application.c:520
msgid "PLUGIN"
msgstr "CONNECTOR"

#: src/calls-application.c:525
msgid "Whether to present the main window on startup"
msgstr "Indica si s'ha de presentar la finestra principal a l'inici"

#: src/calls-application.c:531
msgid "Dial a number"
msgstr "Marqueu un número"

#: src/calls-application.c:532
msgid "NUMBER"
msgstr "NÚMERO"

#: src/calls-call-display.c:246
msgid "Calling…"
msgstr "S'està trucant..."

#: src/calls-call-record-row.c:39 src/calls-manager.c:547
msgid "Anonymous caller"
msgstr "Trucada anònima"

#: src/calls-call-record-row.c:112
#, c-format
msgid ""
"%s\n"
"yesterday"
msgstr "%s\n"
"ahir"

#: src/calls-main-window.c:115
msgid "translator-credits"
msgstr "Marc Riera <marcriera@softcatala.org>"

#: src/calls-main-window.c:173
msgid "Can't place calls: No SIM card"
msgstr "No es poden fer trucades: no hi ha cap targeta SIM"

#: src/calls-main-window.c:178
msgid "Can't place calls: No backend service"
msgstr "No es poden fer trucades: no hi ha cap servei de rerefons"

#: src/calls-main-window.c:182
msgid "Can't place calls: No plugin"
msgstr "No es poden fer trucades: no hi ha cap connector"

#: src/calls-main-window.c:210 src/ui/call-display.ui:280
msgid "Dial Pad"
msgstr "Teclat"

#: src/calls-main-window.c:219
msgid "Recent"
msgstr "Recents"

#: src/ui/call-display.ui:33
msgid "Incoming phone call"
msgstr "Trucada telefònica entrant"

#: src/ui/call-display.ui:126
msgid "Mute"
msgstr "Silencia"

#: src/ui/call-display.ui:163
msgid "Speaker"
msgstr "Altaveu"

#: src/ui/call-display.ui:199
msgid "Add call"
msgstr "Afegeix una trucada"

#: src/ui/call-display.ui:244
msgid "Hold"
msgstr "Posa en espera"

#: src/ui/call-display.ui:336
msgid "Hang up"
msgstr "Penja"

#: src/ui/call-display.ui:367
msgid "Answer"
msgstr "Contesta"

#: src/ui/call-display.ui:449
msgid "Hide the dial pad"
msgstr "Amaga el teclat"

#: src/ui/call-record-row.ui:68
msgid "Call the party"
msgstr "Truca al grup"

#: src/ui/call-record-row.ui:112
msgid "_Delete Call"
msgstr "_Suprimeix la trucada"

#: src/ui/call-selector-item.ui:31
msgid "+441234567890"
msgstr "+441234567890"

#: src/ui/call-selector-item.ui:44
msgid "On hold"
msgstr "En espera"

#: src/ui/encryption-indicator.ui:23
msgid "This call is not encrypted"
msgstr "Aquesta trucada no està xifrada"

#: src/ui/encryption-indicator.ui:45
msgid "This call is encrypted"
msgstr "Aquesta trucada està xifrada"

#: src/ui/history-box.ui:23
msgid "No Recent Calls"
msgstr "No hi ha trucades recents"

#: src/ui/history-header-bar.ui:8
msgid "Recent Calls"
msgstr "Trucades recents"

#: src/ui/history-header-bar.ui:21
msgid "New call…"
msgstr "Trucada nova..."

#. Translators: tooltip for the application menu button
#: src/ui/history-header-bar.ui:40
msgid "Menu"
msgstr "Menú"

#: src/ui/history-header-bar.ui:71
msgid "About Calls"
msgstr "Quant al Trucades"

#: src/ui/main-window.ui:36
msgid "No modem found"
msgstr "No s'ha trobat cap mòdem"

#: src/ui/main-window.ui:56
msgid "Contacts"
msgstr "Contactes"

#: src/ui/new-call-box.ui:101
msgid "Dial"
msgstr "Teclat"

#: src/ui/new-call-box.ui:129
msgid "Backspace through number"
msgstr "Retrocedeix pel nombre"

#: src/ui/new-call-header-bar.ui:8
msgid "New Call"
msgstr "Trucada nova"

#: src/ui/new-call-header-bar.ui:21
msgid "Back"
msgstr "Enrere"
